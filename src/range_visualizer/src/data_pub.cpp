#include <ros/ros.h>
#include <sensor_msgs/Range.h>
#include <cstdlib>
#include <math.h>
#include <std_msgs/Int32.h>
#include <vector>
#include <iostream>

double m_range = 0.0;
std::vector<double> m_ranges(10, 0.0);
float min_range = 0.02;
float max_range = 4;

void convertData(std_msgs::Int32 input_data)
{
  m_range = input_data.data / (double)1000;
  m_ranges.erase(m_ranges.begin());
  m_ranges.push_back(m_range);
}

double filterData()
{
  double sum = 0;
  std::cout << m_ranges.size() << '\n';
  for (int i = 0; i < m_ranges.size(); i++){
    sum += m_ranges[0];
  }
  return sum/10;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "data_pub");

  ros::NodeHandle n;
  ros::Publisher raw_range_pub = n.advertise<sensor_msgs::Range>("ultrasound/raw", 0);
  ros::Publisher filt_range_pub = n.advertise<sensor_msgs::Range>("ultrasound/filtered", 0);
  ros::Subscriber range_sub = n.subscribe<std_msgs::Int32>("range_find", 1, convertData);
  
  ros::Rate loop_rate(100);

  //std::srand(1);

  while (n.ok())
  {
    
    ros::Time current_time = ros::Time::now();

    // https://stackoverflow.com/a/17798317
    //float m_range = min_range + (std::rand() / (RAND_MAX / (max_range - min_range)));

    sensor_msgs::Range range_msg;
    range_msg.header.stamp = current_time;
    range_msg.header.frame_id = "range_viz";

    range_msg.radiation_type = 1;
    range_msg.field_of_view = M_PI/12;
    
    range_msg.min_range = min_range;
    range_msg.max_range = max_range;

    range_msg.range = m_range;
    
    raw_range_pub.publish(range_msg);

    range_msg.range = filterData();

    filt_range_pub.publish(range_msg);


    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}
