#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "motoman_triangle");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    static const std::string PLANNING_GROUP = "sia5";

    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
    
    const robot_state::JointModelGroup* joint_model_group =
    	move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

    namespace rvt = rviz_visual_tools;
    moveit_visual_tools::MoveItVisualTools visual_tools("base_link");
    visual_tools.deleteAllMarkers();

    visual_tools.loadRemoteControl();

    Eigen::Affine3d text_pose = Eigen::Affine3d::Identity();
    text_pose.translation().z() = 1.75;
    visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);

    visual_tools.trigger();

    // moving

    geometry_msgs::Pose target_pose = move_group.getCurrentPose().pose;
    
    std::vector<geometry_msgs::Pose> waypoints;
    waypoints.push_back(target_pose);
    
    target_pose.position.x -= 0.2;
    waypoints.push_back(target_pose);
    move_group.setPoseTarget(target_pose);
    move_group.move();

    target_pose.position.x += 0.2;
    target_pose.position.y += 0.2;
    target_pose.position.z -= 0.2;
    waypoints.push_back(target_pose);
    move_group.setPoseTarget(target_pose);
    move_group.move();

    target_pose.position.z += 0.2;
    target_pose.position.y -= 0.2;
    waypoints.push_back(target_pose);
    move_group.setPoseTarget(target_pose);
    move_group.move();
    
    move_group.setMaxVelocityScalingFactor(0.1);

    moveit_msgs::RobotTrajectory trajectory;
    const double jump_threshold = 0.0;
    const double eef_step = 0.01;
    double fraction = move_group.computeCartesianPath(waypoints, eef_step, jump_threshold, trajectory);
    ROS_INFO_NAMED("motoman_triangle", "Moving triangle", fraction * 100.0);

    move_group.move();

    visual_tools.deleteAllMarkers();
    visual_tools.publishText(text_pose, "Joint Space Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishPath(waypoints, rvt::LIME_GREEN, rvt::SMALL);
    for (std::size_t i = 0; i < waypoints.size(); ++i)
        visual_tools.publishAxisLabeled(waypoints[i], "pt" + std::to_string(i), rvt::SMALL);
    visual_tools.trigger();
    // visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");



    ros::shutdown();
    return 0;

}
