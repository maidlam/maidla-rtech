#include <ros.h>
#include <std_msgs/Int32.h>

// Ultrasonic sensor pins
int echoPin = A4;
int trigPin = A5;

ros::NodeHandle nh;

std_msgs::Int32 dist_msg;
ros::Publisher range_find("range_find", &dist_msg);

void setup()
{
  Serial.begin(9600);
  pinMode(echoPin,INPUT);
  pinMode(trigPin,OUTPUT);

  nh.initNode();
  nh.advertise(range_find);
}

long getSonarReadingMillimeters()
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  long duration_us = pulseIn(echoPin, HIGH);
  long distance_mm = (duration_us / 58.0) * 10;
  return distance_mm;
}

void loop()
{
  long us = getSonarReadingMillimeters(); //Get distance from wall with ultrasonic sensor
  //Serial.println(us);
  dist_msg.data = us;
  range_find.publish( &dist_msg );
  nh.spinOnce();
  delay(20);
}
